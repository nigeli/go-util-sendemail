package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/smtp"
	"os"
	"path/filepath"

	"github.com/go-ini/ini"
)

var _ = flag.Arg
var _ = fmt.Print
var _ = ini.DEFAULT_SECTION
var _ = os.Args

// ServerConf struct of mail server configuration
type ServerConf struct {
	Server string
	Port   int
}

// MessageConf struct of email message configuration
type MessageConf struct {
	From   string
	To     string
	Source string
}

// OptionConf struct of sending email configuration
type OptionConf struct {
	Iteration   int
	Interval    int
	Pick        int
	SubDirLayer int
}

// SendConf total configuration
type SendConf struct {
	server  ServerConf
	message MessageConf
	option  OptionConf
}

func loadConf(filePath string) (conf *SendConf, err error) {
	cfg, err := ini.Load(filePath)
	if err != nil {
		return
	}

	serverConf := &ServerConf{Port: 25}
	err = cfg.Section("SMTP").MapTo(serverConf)
	if err != nil {
		return
	}

	messageConf := new(MessageConf)
	err = cfg.Section("MAIL").MapTo(messageConf)
	if err != nil {
		return
	}

	optionConf := new(OptionConf)
	err = cfg.Section("OPTION").MapTo(optionConf)
	if err != nil {
		return
	}

	conf = &SendConf{
		server:  *serverConf,
		message: *messageConf,
		option:  *optionConf,
	}
	return
}

func sendEmail(conf *SendConf) (count int, err error) {
	count = 0

	fileInfo, err := os.Stat(conf.message.Source)
	if err != nil {
		fmt.Println(err)
		return
	}

	fileLists := make([]string, 0, 100)
	switch mode := fileInfo.Mode(); {
	case mode.IsDir():
		err = filepath.Walk(conf.message.Source, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.Mode().IsRegular() {
				log.Printf("get regular file %s", path)
				fileLists = append(fileLists, path)
			}
			return nil
		})
		if err != nil {
			log.Printf("walk %s error, %v", conf.message.Source, err)
			return 0, err
		}
	case mode.IsRegular():
		fileLists = append(fileLists, conf.message.Source)
	default:
		log.Fatalf("source %s not valid", conf.message.Source)
	}
	log.Printf("get %d sample file totally", len(fileLists))

	addr := fmt.Sprintf("%s:%d", conf.server.Server, conf.server.Port)
	mailFrom := conf.message.From
	rcptTos := []string{conf.message.To}

	for _, sample := range fileLists {
		content, err := ioutil.ReadFile(sample)
		if err != nil {
			log.Printf("read file %s error: %v.", sample, err)
			continue
		}

		client, err := smtp.Dial(addr)
		if err != nil {
			log.Printf("SMTP telnet %s error, %v.", addr, err)
			continue
		}
		defer client.Close()

		if err := client.Mail(mailFrom); err != nil {
			log.Printf("SMTP mail from %s error, %v.", mailFrom, err)
			continue
		}

		validRcpt := 0
		for _, rcpt := range rcptTos {
			if err := client.Rcpt(rcpt); err != nil {
				log.Printf("SMTP rcpt to %s error, %v.", rcpt, err)
			} else {
				validRcpt++
			}
		}
		if validRcpt == 0 {
			log.Printf("SMTP must be at least one rcpt.")
			continue
		}

		writer, err := client.Data()
		if err != nil {
			log.Printf("SMTP data error, %v.", err)
			continue
		}
		_, err = writer.Write(content)
		if err != nil {
			log.Printf("SMTP data transfer error, %v.", err)
			continue
		}

		if err = writer.Close(); err != nil {
			log.Printf("SMTP writer close error, %v.", err)
			continue
		}
		if err = client.Quit(); err != nil {
			log.Printf("SMTP client quit error, %v.", err)
			continue
		}

		count++
		log.Printf("send success %s", sample)
	}
	return
}

func main() {
	confPath := flag.String("c", "sendemail.ini", "configuration path")
	logPath := flag.String("l", "", "log path")
	flag.Parse()

	if *logPath == "" {
		log.SetOutput(os.Stdout)
	}

	conf, err := loadConf(*confPath)
	if err != nil {
		log.Fatalln("load configuration error,", err)
	}

	count, err := sendEmail(conf)
	if err != nil {
		log.Fatalln("send email error,", err)
	}
	log.Printf("send %d email totally", count)
	log.Printf("done")
	return
}
